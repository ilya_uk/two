#include <iostream>
#include <FeatureExtraction.h>
#include "Visualisation.h"

using namespace std;
using namespace cv;
using namespace fe;

int main() 
{
	cout << "hello world!" << endl;
	cout << fe::GetTestString().c_str() << endl;

	auto PM = fe::CreatePolynomialManager();
	int n_max;
	int diametr = 70;

	cout << "Enter an order of the polinomial:" << endl;
	cin >> n_max;
	cout << endl;


	PM->InitBasis(n_max, diametr);
	OrthoBasis basis = PM->GetBasis();
	ShowPolynomials("Basis Legendre", basis);
	waitKey(0);
	cout << PM->GetType() << endl;

	// ��������� �����������
	Mat mat = imread("image.png");
	// ���������� ���� ��������
	Show64FC1Mat("Image", mat);
	waitKey(0);

	// ��������� ��������: ����� ����
	auto IPB = fe::CreateBlobProcessor();

	Mat mat_bin, mat_1;
	cvtColor(mat, mat_1, CV_RGB2GRAY, 1);

	// ��������� � ������ ������ (8 ���)
	mat *= 255;
	mat.convertTo(mat, CV_8UC1);
	// ����������
	threshold(mat_1, mat_bin, 100, 255, THRESH_BINARY);

	// ���� �����
	vector<Mat> blobs;
	vector<Mat>norm_blobs;
	IPB->DetectBlobs(mat_bin, blobs);

	// �������� �� � ������ �������
	IPB->NormalizeBlobs(blobs, norm_blobs, diametr);

	cout << IPB->GetType() << endl;
	
	vector<ComplexMoments> mat_decomp(norm_blobs.size());
	vector<Mat> mat_recovery(norm_blobs.size());

	for (int i = 0; i < norm_blobs.size(); i++)
	{
		// ������������ ��������
		PM->Decompose(norm_blobs[i], mat_decomp[i]);

		// �������� �������
		PM->Recovery(mat_decomp[i], mat_recovery[i]);

		// ���������� �������� ��������� ����� � ��������� � ��������������
		ShowBlobDecomposition("", norm_blobs[i], mat_recovery[i]);
		waitKey(0);
	}

	system("pause");
	return 0;
}