/*****����������� ���������*****/
#pragma once
#include "PolynomialManager.h"

using namespace fe;
using namespace std;
using namespace cv;

class PolynomialManager_r :
	public PolynomialManager
{
	OrthoBasis polynomials;

public:
	PolynomialManager_r();
	~PolynomialManager_r();
	/**��������� �������� � ��� �� ���������
	* @param blob - �������� (������� �������), ������ ���� ���� CV_8UC1
	* @param decomposition - ����� ��� ������ ����������
	*/
	void Decompose(cv::Mat blob, ComplexMoments & decomposition);

	/**������������ �������� �� ����������.
	* @param decomposition - ���������� �������� � ���.
	* @param recovery - ����� ��� ������ ���������������� �����������.
	*					 ���������������� ����������� ����� ��� CV_64FC1
	*/
	void Recovery(ComplexMoments & decomposition, cv::Mat & recovery);

	/**������������������� ����� ������������� ��������� ~ exp(jm*fi)
	* @param n_max - ������������ ���������� ������� ���������.
	* @param diameter - ������� ����������, �� ������� ����� ������������� ��������, �������.
	*/
	void InitBasis(int n_max, int diameter);

	/**�������� �������� ������� ��� ������ � ����������.
	* @return - �������� �������.
	*/
	string GetType();

	/**�������� ����� ������������� ���������.
	* @return ����� ������������� ���������. ������ ������� ����������� std::pair<cv::Mat, cv::Mat>.
	*		   � ���� first �������� �������� ����� ��������, � ���� second ������. ������ ����� ����� ��� CV_64FC1.
	*/
	OrthoBasis GetBasis();
};

