
#include "PolynomialManager_r.h"


PolynomialManager_r::PolynomialManager_r()
{
}


PolynomialManager_r::~PolynomialManager_r()
{
}

OrthoBasis PolynomialManager_r::GetBasis()
{
	return polynomials;
}

/**��������� �������� � ��� �� ���������
* @param blob - �������� (������� �������), ������ ���� ���� CV_8UC1
* @param decomposition - ����� ��� ������ ����������
*/
void PolynomialManager_r::Decompose(cv::Mat blob, ComplexMoments & decomposition)
{
	decomposition.re = Mat::Mat((int)polynomials.size(), (int)polynomials.size(), CV_64FC1); 
	decomposition.im = Mat::Mat((int)polynomials.size(), (int)polynomials.size(), CV_64FC1);

	int diametr = polynomials[0][0].first.size().width;

	Mat M_1 = Mat::Mat(diametr, diametr, CV_64FC1, Scalar(-1, 0,0,0));

	for (int i = 0; i < polynomials.size(); i++)
	{
		for (int j = 0; j < polynomials[i].size(); j++)
		{
			Mat V;
			blob.convertTo(V, CV_64FC1);
			V *= 2 / 255.;
			V = V + M_1;
			(decomposition.re).at<double>(i, j) = sum(V.mul(polynomials[i][j].first))[0] / diametr / diametr;
			(decomposition.im).at<double>(i, j) = sum(V.mul(polynomials[i][j].second))[0] / diametr / diametr;
		}
	}
}

/**������������ �������� �� ����������.
* @param decomposition - ���������� �������� � ���.
* @param recovery - ����� ��� ������ ���������������� �����������.
*					 ���������������� ����������� ����� ��� CV_64FC1
*/
void PolynomialManager_r::Recovery(ComplexMoments & decomposition, cv::Mat & recovery)
{
	Mat mat = Mat::zeros(polynomials[0][0].first.size().width, polynomials[0][0].first.size().width, CV_64FC1);

	for (int i = 0; i < polynomials.size(); i++)
	{
		for (int j = 0; j < polynomials[i].size(); j++)
		{
			mat += polynomials[i][j].first * (decomposition.re).at<double>(i, j);
			mat += polynomials[i][j].second * (decomposition.im).at<double>(i, j);
		}
	}

	recovery = mat;
}



/********�������� ��������*******/
/**������������������� ����� ������������� ��������� ~ exp(jm*fi)
* @param n_max - ������������ ���������� ������� ���������.
* @param diameter - ������� ����������, �� ������� ����� ������������� ��������, �������.
*/
void PolynomialManager_r::InitBasis(int n_max, int diameter)
{
	double koef = 2. / diameter;
	polynomials.resize(n_max + 1);
	for (int i = 0; i < n_max + 1; i++)
	{
		polynomials[i].resize(n_max + 1);
		for (int j = 0; j <= n_max; j++)
		{

			polynomials[i][j].first = cv::Mat::zeros(diameter, diameter, CV_64FC1);
			polynomials[i][j].second = cv::Mat::zeros(diameter, diameter, CV_64FC1);

			double xf = 1.;
			for (int x = 0; x < diameter; x++)
			{
				double yf = 1.;
				for (int y = 0; y < diameter; y++)
				{

					double r = sqrt((x - diameter / 2)*(x - diameter / 2) + (y - diameter / 2)*(y - diameter / 2))*koef;

					double fi = atan2((y - diameter / 2), (x - diameter / 2));

					double f = rf::RadialFunctions::ShiftedLegendre(r, i);

					polynomials[i][j].first.at<double>(x, y) = f*cos(j*fi);
					polynomials[i][j].second.at<double>(x, y) = f*sin(j*fi);

				}

			}
		}

	}


}

/**�������� �������� ������� ��� ������ � ����������.
* @return - �������� �������.
*/
std::string PolynomialManager_r::GetType()
{
	return "Polynomials Legendre by Borisuyk Ilya";
}