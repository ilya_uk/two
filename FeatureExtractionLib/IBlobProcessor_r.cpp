
#include "IBlobProcessor_r.h"


IBlobProcessor_r::IBlobProcessor_r()
{
	;
}


IBlobProcessor_r::~IBlobProcessor_r()
{
	;
}

/**�������� �������� ������������� ����������� ������� ��������.
* @return ��������
*/
string IBlobProcessor_r::GetType()
{
	return "Detect and normalize blobs";
}

/**����� ������� ������� �� �����������
* @param image - ����������� ��� ������ ������� ��������.
*				 ������ ����� ��� CV_8UC1
* @param blobs - ����� ��� ������ �������������������� ������� ��������
*/
void IBlobProcessor_r::DetectBlobs(cv:: Mat image, std::vector<cv::Mat>& blobs)
{
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	/// Find contours
	findContours(image, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	Point2f centre;
	float radius;
	Scalar color = 255;

	blobs.clear();

	int idx = 1;// �������� � 1, ����� �� ��������� ������� ������������� ������
	for (; idx >= 0; idx = hierarchy[idx][0])
	{
		// ���������� ������� ��������
		minEnclosingCircle(contours[idx], centre, radius);
		Mat drawing = Mat::zeros(2 * radius, 2 * radius, CV_8UC1);
		// ������ � ������ ��������� �����
		drawContours(drawing, contours, idx, color, CV_FILLED, 8, hierarchy, 8, Point(radius - centre.x, radius - centre.y));

		// ������, ���������� �� �������������� ��������
		Mat drawing2 = Mat::zeros(2 * radius, 2 * radius, CV_64FC1);
		drawing.convertTo(drawing2, CV_64FC1);

		Moments mm = moments(drawing2, true);
		// ����� ����
		double c_x = mm.m10 / mm.m00;
		double c_y = mm.m01 / mm.m00;

		double I0 = sum(drawing2)[0];
		
		// ������� ������������� ��� ���. ��������
		Mat M3 = Mat::Mat(2 * radius, 2 * radius, CV_64FC1);
		for (int i = 0; i < M3.size().width; i++)
		{
			for (int j = 0; j < M3.size().width; j++)
			{
				M3.at<double>(i, j) = sqrt((i - c_y)*(i - c_y) + (j - c_x)*(j - c_x));
			}
		}
		// ���. ��������
		double ro = sum(M3.mul(drawing2))[0] / I0;

		//������� ������������� ��� ���������� ���������
		Mat M4 = Mat::Mat(2 * radius, 2 * radius, CV_64FC1);
		for (int i = 0; i < M4.size().width; i++)
		{
			for (int j = 0; j < M4.size().width; j++)
			{
				M4.at<double>(i, j) = (sqrt((i - c_y)*(i - c_y) + (j - c_x)*(j - c_x)) - ro)*(sqrt((i - c_y)*(i - c_y) + (j - c_x)*(j - c_x)) - ro);
			}
		}
		// ���������
		double sigma = sum(M4.mul(drawing2))[0] / I0;

		// ��������� ������ �����
		int R0 = (int)(ro + 2.8*sqrt(sigma));
		// �������� ��������
		Mat drawing3 = Mat::zeros(2 * R0, 2 * R0, CV_8UC1);
		// ������������ ����������
		drawContours(drawing3, contours, idx, color, CV_FILLED, 8, hierarchy, 8, Point(R0 - centre.x + radius - c_x, R0 - centre.y + radius - c_y));

		// ������� � ������� ������� ���� �������
		Mat drawing4 = Mat::zeros(2 * R0, 2 * R0, CV_8UC1);
		// ���� ��������
		
		double mu11 = mm.m11 / mm.m00 - c_x*c_y;
		double mu20 = mm.m20 / mm.m00 - c_x*c_x;
		double mu02 = mm.m02 / mm.m00 - c_y*c_y;
		// ���������, ������ �� �������� ��������� ������� �������
		double theta = 0.5 * atan(2 * mu11 / (mu20 - mu02));

		// ������� �������� 
		Mat rot_mat;
		if (mu20 - mu02 < 0)
		 	rot_mat = getRotationMatrix2D(Point2f(R0, R0), theta / 3.1416 * 180, 1);
		else if (mu11 < 0)
			rot_mat = getRotationMatrix2D(Point2f(R0, R0), theta / 3.1416 * 180 + 90, 1);
		else rot_mat = getRotationMatrix2D(Point2f(R0, R0), theta / 3.1416 * 180 - 90, 1);
		// �������� ��������������
		warpAffine(drawing3, drawing4, rot_mat, drawing3.size());
		
		blobs.push_back(drawing4);
		
	}

}

/**�������� ������ ������� �������� � ������� ��������.
* @param blobs - ������� �������
* @param normilized_blobs - ����� ��� ������ ������� �������� ������� �������.
* @param side - ������� �������� �� ������� ����� ���������� ��������������� ������� �������.
*/
void IBlobProcessor_r::NormalizeBlobs(std::vector<cv::Mat> & blobs, std::vector<cv::Mat> & normalized_blobs, int side)
{
	normalized_blobs.clear();
	for (int i = 0; i < blobs.size(); i++)
	{
		Mat mat_fixed_size = Mat::zeros(side, side, CV_8UC1);
		Mat mat_gauss = Mat::zeros(side, side, CV_8UC1);
		// ������� ������
		double s0 = 0.5 * ((double)blobs[i].size().width / side) * ((double)blobs[i].size().width / side);
		int s;
		if (s0 < 1) s = 1;
		else if (s0 < 3) s = 3;
		else s = 5;
		GaussianBlur(blobs[i], mat_gauss, Size(s, s), 2, 2);

		resize(mat_gauss, mat_fixed_size, Size(side, side), 0, 0, INTER_CUBIC);
		normalized_blobs.push_back(mat_fixed_size);
	}
}