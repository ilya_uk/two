#define ANNDLL_EXPORTS
#include "FeatureExtraction.h"

std::string fe::GetTestString()
{
	return "You successfuly plug feature extraction library!";
}

std::shared_ptr<fe::IBlobProcessor> fe::CreateBlobProcessor()
{

	return std::shared_ptr<fe::IBlobProcessor>(new IBlobProcessor_r());

	//throw "Stub called!";
	//return NULL;
}

std::shared_ptr<fe::PolynomialManager> fe::CreatePolynomialManager()
{
	return std::shared_ptr<fe::PolynomialManager>(new PolynomialManager_r());
	
	//throw "Stub called!";
	//return NULL;
}