
#pragma once
#include "IBlobProcessor.h"

using namespace fe;
using namespace cv;
using namespace std;

class IBlobProcessor_r :
	public IBlobProcessor
{
public:
	IBlobProcessor_r();
	~IBlobProcessor_r();
	std::string GetType();
	void DetectBlobs(cv::Mat image, std::vector<cv::Mat>& blobs);
	void NormalizeBlobs(
		std::vector<cv::Mat> & blobs,
		std::vector<cv::Mat> & normalized_blobs,
		int side
		);
};
